# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This for sample announcement Creating and viewing panel only authenticated user can only create/edit/delete the announcement and there is one API for list down all the active announcements 
* used laravel 8 and php 7
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

* Summary of set up
```
git clone https://danushkaperera@bitbucket.org/danushkaperera/ivvy-test.git

```
 run this command in your command 

* go to the project folder
```
cd ivvy-test

```
* download all required packages 
```
composer update

```
* set up the env file wirh database details and create new database, and then migrate all the migration files with below command
```
php artisan migrate and php artisan serve

```
* After everything setup go to the localhost url 

* For announcement Active/Innactive scheduling run by the below command 
```

php artisan schedule:work

```
## Unit Testing  ##

* All test files are inside the tests/Feature folder you can run all the test cases with running below command 
```

vendor/bin/phpunit

```






### Development Methodologies ###

* used laravel repository for reduce controller coding and use integrated application security libraries 
* I used laravel framework because that is MVC framework, easy to understand for developers. There are some design patters to develope clean code with less stress  
* I used password encryption for security perposes and integrated login attempt handler

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

