<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Announcement extends Model
{
    use HasFactory, Notifiable;

        protected $guarded = [];

        protected $fillable = [
        'title',
        'content',
        'start_date',
        'end_date',
        'active',
    ];
}
