<?php

namespace Database\Factories;

use App\Models\Announcement;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
class AnnouncementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Announcement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        
       return [
            'title' => $this->faker->title(),
            'content' => $this->faker->paragraph(),
            'start_date' =>$this->faker->unique()->dateTimeBetween($startDate = 'now', $endDate = '30 years', $timezone = null),
            'end_date' => $this->faker->unique()->dateTimeBetween($startDate = 'now', $endDate = '30 years', $timezone = null),
            'active'=>1,
            
        ];
    }
}
