<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Announcement;
use App\Models\User;
class AnnouncementTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
     public function test_announcement_list_cant_be_able_to_open_for_guest_user()
    {
        $response = $this->get('/home');

        $response->assertRedirect('/login');
    }

     public function test_announcement_list_screen_open_for_authenticated_user(){
          $user = User::factory()->create();
          $response = $this->actingAs($user, 'web')
          ->get('/home');
       
       $response->assertStatus(200);

    }
     public function test_announcement_create_screen_open_for_guest_user(){
        $response = $this->get('/announcement/create');
       
       $response->assertStatus(302);

    }

     public function test_announcement_create_screen_open_for_authenticated_user(){
          $user = User::factory()->create();
          $response = $this->actingAs($user, 'web')
          ->get('/announcement/create');
       
       $response->assertStatus(200);

    }
    public function test_announcement_store_with_authenticated_user(){
        $user = User::factory()->create();
        $Announcement = Announcement::factory()->make();
        $response = $this->actingAs($user, 'web')
         ->post('/announcement/store/',$Announcement->toArray());
       
        $response->assertRedirect('/home');


    }

     public function test_announcement_edit_screen_open_for_authenticated_user(){
          $user = User::factory()->create();
          $Announcement = Announcement::factory()->create();
          $response = $this->actingAs($user, 'web')
          ->get('/announcement/edit/'.$Announcement->id);
       
        $response->assertStatus(200);

    } 
     
    
}
