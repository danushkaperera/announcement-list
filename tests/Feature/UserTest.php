<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


use App\Models\User;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_login_screen()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

        public function test_user_login()
    {
        $user = User::factory()->create();
        $response = $this->post('/login');
        $response = $this->actingAs($user, 'web')
          ->get('/home');

       $response->assertStatus(200);
    }

   

    public function test_user_registration()
    {
        $response = $this->post(route('register'), [
            "name" => "John Doe",
            "email" => "doe@example.com",
            "password" => "demo12345",
            "password_confirmation" => "demo12345"
        ]);
        $response->assertRedirect(route('home'));
   
    }
}
