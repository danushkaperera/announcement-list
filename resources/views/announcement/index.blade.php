@extends('layouts.app')


@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Announcements</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('announcement.create') }}"> Create New Announcement</a>
            </div>
            <br/>
        </div>
    </div>
</div>

<div class="container">
@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
 <tr>
   <th>#ID</th>
   <th>Title</th>
   <th>Content</th>
   <th>Start Date</th>
   <th>End Date</th>
   <th>Publish</th>
   <th>Action</th>
 </tr>
 @foreach ($announcements as $key => $announcement)
  <tr>
    <td>{{ ++$i }}</td>
    <td class="w-25">{{ $announcement->title }}</td>
    <td class="w-25">{{ $announcement->content }}</td>
    <td class="w-25">{{ \Carbon\Carbon::parse($announcement->start_date)->format('Y-m-d') }}</td>
    <td class="w-25">{{ \Carbon\Carbon::parse($announcement->end_date)->format('Y-m-d') }}</td>

    <td class="w-25">@if ($announcement->active ==1) <span class="badge badge-primary">{{"Active"}}</span>@else<span class="badge badge-secondary">{{"Innactive"}}</span>    @endif  </td>
 
    
    <td>
       <a class="btn btn-primary" href="{{ route('announcement.edit',$announcement->id) }}">Edit</a>
        {!! Form::open(['method' => 'DELETE','route' => ['announcement.destroy', $announcement->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </td>
  </tr>
 @endforeach
</table>


{!! $announcements->render() !!}
</div>

@endsection