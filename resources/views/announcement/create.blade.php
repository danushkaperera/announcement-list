@extends('layouts.app')


@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Create Announcements</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-secondary" href="{{ route('home') }}"> back</a>
            </div>
            <br/>
        </div>
    </div>
</div>



<div class="container">
			@if (count($errors) > 0)
		  <div class="alert alert-danger">
		    <strong>Whoops!</strong> There were some problems with your input.<br><br>
		    <ul>
		       @foreach ($errors->all() as $error)
		         <li>{{ $error }}</li>
		       @endforeach
		    </ul>
		  </div>
			@endif

<form method="post" action="{{ route('announcement.store') }}">
	@csrf
	<div class="form-row">
	  <div class="form-group col-md-6">
	    <label for="formGroupExampleInput">Title</label>
	    <input type="text" class="form-control" name="title"  placeholder="Title">
	  </div>
	</div>
  <div class="form-row">
	  <div class="form-group col-md-12">
	    <label for="formGroupExampleInput2">Content</label>
	    <input type="text" class="form-control" name="content"  placeholder="Content">
	  </div>
  </div>
  <div class="form-row">
	   <div class="form-group col-md-6">
	    <label for="formGroupExampleInput2">Start Date</label>
	    <input type="text" data-provide="datepicker" class="form-control" name="start_date" id="start_date" >
	   </div>
	   <div class="form-group col-md-6">
	    <label for="formGroupExampleInput2">End Date</label>
	    <input type="text" data-provide="datepicker" class="form-control" name="end_date" id="end_date" >
  </div>
</div>

  <br/>
    <button type="submit" class="btn btn-primary">Submit</button>
    
</form>


</div>

@endsection